vk_deps
=========

Installs Vulkan SDK (if not already) as a build-step and exports the required
environment variables for `cargo` to build against it.

`vk_deps` has been tested to work on MacOS and Linux. Windows support is possible, but Vulkan for
Windows is shipped differently (installer) than other platforms (extractable). If you have knowledge
and time, please feel free to submit a PR. In the mean time, installing Vulkan using the provided
installer is the suggested installation method for Windows.

iOS and Android should also be supported as-is, but have not been tested.

Requirements
=============

- `pkg-config`
- OpenSSL headers
    - Debian/Ubuntu: `libssl-dev`
    - Fedora/RHEL/Centos: `openssl-devel`
    - MacOS (brew): `openssl`



Usage
===========

This library is intended to be used in a `build.rs` script, but may be adapted to other uses.

### Example

```
// build.rs

extern crate vk_deps;

const VULKAN_VERSION: &'static str = "1.1.106.0";

fn main() {
    // ensure the specified vulkan version is installed
    let sdk = vk_deps::ensure(VULKAN_VERSION.to_string())
        .expect("failed to install Vulkan SDK");


    // emits cargo/rustc environment variables that set all needed env vars
    sdk.print_cargo_env();
    // emits a shell file at $VULKAN_SDK/env.sh that contains the needed exports to
    // find the Vulkan installation
    sdk.write_env_script();
}
```


Optional Environment Variables
================================

Configuration of the installation can be done through the environment.

These options are in the environment and not code because they are options which should
not persist in version control or even between developers. They are "personal choice"
options in the case the defaults are not ideal.

All toggles support either `1` or `true` as on/true with all other values being off/false.


### `VULKAN_INSTALL_DEST`

When set (to a valid string), the SDK will be extracted/installed as a child of this directory.

The version being extracted/installed will always be appended to the destination.
The value of this variable must remain consistent across `cargo` runs or you may find that
Vulkan is being reinstalled constantly or simply not found.


### `VULKAN_REINSTALL`

Reinstalls Vulkan even if there exists an installation for this version.

The original directory is removed (if it existed) and a fresh copy is installed in its place.


### `VULKAN_KEEP_SOURCE`

Keeps the file we downloaded for installation instead of deleting it.

The file is saved as a sibling to the install path. For example:

```
$HOME
├── .vulkan_sdk
│   ├── vulkan-1.1.106.0-linux.tar.gz
│   ├── 1.1.106.0/
│   │   ├── vulkan
│   │   ├── sdk
│   │   ├── files
│   │   └── here
```

When installing, whether providing `VULKAN_REINSTALL=1` or a first-time install, we look
for the source file to save time and bandwidth.

If this file becomes corrupted, it is the user's responsibility to delete it which clears
the bad state and installation can continue from remote a file.
