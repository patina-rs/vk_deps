//
// macos
//

#[cfg(target_os = "macos")]
mod os_diff {
    pub fn os_name() -> String { "mac".to_string() }
    pub fn os_long_name() -> String { "macos" .to_string()}
    pub fn vk_extension() -> String { "tar.gz".to_string() }
    pub fn vk_versioned_filename(ver: &String) -> String {
        format!("vulkansdk-{os}-{ver}.{ext}",
            os=os_long_name(),
            ver=ver,
            ext=vk_extension(),
        )
    }

    pub fn vulkan_sdk_dir() -> String { "macOS".to_string() }
    pub fn print_cargo_env(sdk_path: &std::path::PathBuf) {
        let as_str = sdk_path.to_str().unwrap();
        vec!(
            ("VK_LAYER_PATH", format!("{}/etc/vulkan/explicit_layer.d",as_str)),
            ("VK_ICD_FILENAMES", format!("{}/etc/vulkan/icd.d/MoltenVK_icd.json",
                as_str)),
            ("DYLD_LIBRARY_PATH", crate::join_env("DYLD_LIBRARY_PATH", format!("{}/lib", as_str))),
        )
    }
}

//
// ios
//

#[cfg(target_os = "ios")]
mod os_diff {
    //
    // NOTE: everything looks the same as the MacOS case here
    //       we do export different variables, however.
    //
    pub fn os_name() -> String { "mac".to_string() }
    pub fn os_long_name() -> String { "macos" .to_string()}
    pub fn vk_extension() -> String { "tar.gz".to_string() }
    pub fn vk_versioned_filename(ver: &String) -> String {
        format!("vulkansdk-{os}-{ver}.{ext}",
            os=os_long_name(),
            ver=ver,
            ext=vk_extension(),
        )
    }

    pub fn vulkan_sdk_dir() -> String { "macOS".to_string() }
    pub fn print_cargo_env(sdk_path: &std::path::PathBuf) {
        let as_str = sdk_path.to_str().unwrap();
        vec!(
            ("VK_LAYER_PATH", format!("{}/etc/vulkan/explicit_layer.d",
                as_str)),
            ("VK_ICD_FILENAMES", format!("{}/../iOS/dynamic/MoltenVK_icd.json",
                as_str)),
        )
    }
}


//
// linux
//

#[cfg(target_os = "linux")]
mod os_diff {
    pub fn os_name() -> String { "linux".to_string() }
    pub fn os_long_name() -> String { "linux".to_string() }
    pub fn vk_extension() -> String { "tar.gz".to_string() }
    pub fn vk_versioned_filename(ver: &String) -> String {
        format!("vulkansdk-{os}-{ver}.{ext}",
            os=os_long_name(),
            ver=ver,
            ext=vk_extension(),
        )
    }

    pub fn vulkan_sdk_dir() -> String { "x86_64".to_string() }
    pub fn cargo_env(sdk_path: &std::path::PathBuf) -> Vec<(&'static str, String)> {
        let as_str = sdk_path.to_str().unwrap();
        vec!(
            ("VK_LAYER_PATH", format!("{}/etc/explicit_layer.d", as_str)),
            ("LD_LIBRARY_PATH", crate::join_env("LD_LIBRARY_PATH", format!("{}/lib", as_str))),
        )
    }
}

//
// android
//
// TODO: seems like a more involved installation process
//       may need some help with this -- also, no device to test on
//



//
// windows
//
// TODO: would rather not use an installer .exe
//       build from source seems bad too....
//

#[cfg(target_os = "windows")]
mod os_diff {
    pub fn os_name() -> String { "windows".to_string() }
    pub fn os_long_name() -> String { "windows".to_string() }
    pub fn vk_extension() -> String { "exe".to_string() }
    pub fn vk_versioned_filename(ver: &String) -> String {
        format!("VulkanSDK-{ver}-Installer.{ext}",
            ver=ver
            ext=vk_extension(),
        )
    }

    pub fn vulkan_sdk_dir() -> String { panic!("unsure of what directory to use with Windows") }
    pub fn print_cargo_env(sdk_path: &std::path::PathBuf) {
        let as_str = sdk_path.to_str().unwrap();
        /*
        vec!(
            ("PATH", crate::join_env("PATH", format!("{}/lib", as_str)))
        )
        */
        panic!("unsure of what to provide rustc on Windows");
    }
}



// expose
pub use os_diff::*;
