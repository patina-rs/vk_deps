extern crate curl;
extern crate dirs;
extern crate flate2;
extern crate tar;

mod os_detect;

use std::io::Write;
use std::error::Error as StdError;


static ENV_INSTALL_DEST: &'static str = "VULKAN_INSTALL_DEST";
static ENV_REINSTALL: &'static str = "VULKAN_REINSTALL";
static ENV_KEEP_SOURCE: &'static str = "VULKAN_KEEP_SOURCE";

static SUCCESS_FILE_FLAG: &'static str = ".success";

type InnerError = (dyn std::error::Error + std::marker::Send + std::marker::Sync);


fn extract_inner_error<'a>(err: &'a std::io::Error) -> &'a InnerError {
    match err.get_ref() {
        Some(e) => { e }
        None => { err }
    }
}

pub enum Error {
    IO(std::io::Error),
    Tar(std::io::Error),
    Env(std::env::VarError),
    Curl(curl::Error),
    Unimplemented(String),
    NoHome
}
impl Error {
    pub fn new_tar(err: std::io::Error) -> Error {
        Error::Tar(err)
    }
}
impl std::error::Error for Error {}
impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
        let as_str = match self {
            Error::IO(e) => { format!("{}", e) }
            Error::Tar(e) => { format!("{}", extract_inner_error(e)) }
            Error::Env(e) => { format!("{}", e) }
            Error::Curl(e) => { format!("{}", e) }
            Error::Unimplemented(e) => { e.clone() }
            Error::NoHome => { "could not find/detect home directory for user".to_string() }
        };

        match self.source() {
            Some(c) => {
                write!(f, "{}: {}", c, as_str)
            }
            None => {
                write!(f, "{}", as_str)
            }
        }
    }
}
impl std::fmt::Debug for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
        write!(f, "{}", self)
    }
}
impl From<std::io::Error> for Error {
    fn from(e: std::io::Error) -> Error {
        Error::IO(e)
    }
}
impl From<curl::Error> for Error {
    fn from(e: curl::Error) -> Error {
        Error::Curl(e)
    }
}
impl From<std::env::VarError> for Error {
    fn from(e: std::env::VarError) -> Error {
        Error::Env(e)
    }
}

fn default_install_root() -> Result<std::path::PathBuf, Error> {
    let home = dirs::home_dir();
    match home {
        Some(h) => { Ok(h) }
        None => { Err(Error::NoHome) }
    }
}

pub fn get_install_root() -> Result<std::path::PathBuf, Error> {
    let mut root = match std::env::var(ENV_INSTALL_DEST) {
        Ok(p) => {
            let mut buf = std::path::PathBuf::new();
            buf.push(p);
            buf
        }
        Err(e) => {
            if std::env::VarError::NotPresent == e {
                default_install_root()?
            } else {
                return Err(e.into()); // bad juju, get out now
            }
        }
    };

    root.push(".vulkan_sdk");
    Ok(root)
}

fn get_tmp_file_path(ver: &String) -> Result<std::path::PathBuf, Error> {
    let mut file_dest = get_install_root()?;
    file_dest.push(os_detect::vk_versioned_filename(&ver));
    Ok(file_dest)
}

fn env_too_bool(key: &'static str) -> bool {
    let val = std::env::var(key).unwrap_or("".to_string());
    match val.as_str() {
        "1" | "true" => { true }
        _ => { false }
    }
}

fn separate_env(key: &'static str, value: String, sep: char) -> String {
    std::env::var(key).map(|v| format!("{}{}{}", value, sep, v)).unwrap_or(value)
}
#[cfg(not(target_os = "windows"))]
fn join_env(key: &'static str, value: String) -> String {
    separate_env(key, value, ':')
}
#[cfg(target_os = "windows")]
fn join_env(key: &'static str, value: String) -> String {
    separate_env(key, value, ';')
}

pub struct SDK {
    version: String,
    fetch_url: String,
    dest: std::path::PathBuf,
}
impl SDK {
    pub fn with_version(ver: &String) -> Result<SDK, Error> {
        let mut dest = get_install_root()?;
        dest.push(ver);

        let fmt_url = SDK::make_fetch_url(ver);
        Ok(SDK {
            version: ver.clone(),
            fetch_url: fmt_url,
            dest: dest,
        })
    }

    fn fetch(&self, tmp_loc: &std::path::PathBuf) -> Result<(), Error> {
        // if the temp file exists, use that instead of downloading
        if tmp_loc.exists() {
            eprintln!("reusing existing source file: {}", tmp_loc.display());
            return Ok(());
        }

        eprintln!("fetching Vulkan SDK from: {}", &self.fetch_url);

        let mut file = std::fs::File::create(tmp_loc)?;
        let mut handle = curl::easy::Easy::new();
        handle.url(&self.fetch_url)?;

        { // scope to make the buffer sharing okay
            let mut tx = handle.transfer();
            tx.write_function(|data| {
                // allow this to panic on write failure -- nothing we can do
                let len = match file.write(data) {
                    Ok(l) => { l }
                    Err(e) => {
                        panic!("failed to write installation source: {}", e);
                    }
                };
                Ok(len)
            })?;
            tx.perform()?;
        }

        file.sync_all()?;
        Ok(())
    }

    pub fn check_if_installed(&self) -> bool {
        if !self.dest.exists() {
            return false;
        }

        let mut test = self.dest.clone();
        test.push(SUCCESS_FILE_FLAG);

        if !test.exists() {
            eprintln!("found dirty install in {}, reinstalling...", self.dest.display());
            return false
        }

        return true;
    }

    fn mark_installed(&self) -> Result<(), Error> {
        let mut test = self.dest.clone();
        test.push(SUCCESS_FILE_FLAG);

        std::fs::File::create(test).map(|_| Ok(()))?
    }

    fn install_gzip(&self, from: &std::path::PathBuf) -> Result<(), Error> {
        let tarball = flate2::read::GzDecoder::new(std::fs::File::open(from)?);
        let mut archive = tar::Archive::new(tarball);
        let entries = archive.entries().expect("could not get entries");

        for (_, entry) in entries.enumerate() {
            let mut file = entry.expect("could not get file from entry");
            let p = file.path().expect("could not get path from file");;

            // skip top-level directory
            let mut anc = p.ancestors();
            if let None = anc.next() { // next to skip self
                continue
            }
            if anc.count() < 2 {
                continue
            }

            // remove the leading directory from this file's output path
            let mut trimmed = p.components();
            trimmed.next(); // skip the then top-level directory

            // append the now-relative path to our destination and unpack
            let mut dest = self.dest.clone();
            dest.push(trimmed.as_path());
            file.unpack(&dest).expect("could not unpack");
        }

        Ok(())
    }

    pub fn install(&self) -> Result<(), Error> {
        let is_installed = self.check_if_installed();
        let reinstall = env_too_bool(ENV_REINSTALL);

        if is_installed && !reinstall {
            eprintln!("found existing Vulkan SDK installation at: {}",
                &self.dest.display());
            return Ok(());
        } else if is_installed && reinstall {
            // clean up the existing directory on reinstall
            std::fs::remove_dir_all(&self.dest)?;
        }

        eprintln!("installing Vulkan SDK v{} to: {}",
            &self.version, &self.dest.display());

        // pre-create any intermediate directories to the install dest
        std::fs::create_dir_all(&self.dest)?;

        eprintln!("starting fetch+install...");
        let tmp_file_loc = get_tmp_file_path(&self.version)?;
        self.fetch(&tmp_file_loc)?;

        let ext = os_detect::vk_extension();
        match ext.as_str() {
            "tar.gz" => {
                eprintln!("starting extraction...");
                self.install_gzip(&tmp_file_loc)?;
                eprintln!("finished extraction...");
            }
            "exe" => {
                return Err(Error::Unimplemented(
                    "Windows exe installation currently unsupported".to_string()));
            }
            _ => {
                return Err(Error::Unimplemented(
                    format!("installation of file type {} unimplemented", ext)));
            }
        }
        eprintln!("finished fetch+install...");
        eprintln!(" fetch+install...");

        if env_too_bool(ENV_KEEP_SOURCE) {
            eprintln!("INFO: keeping the source installation file: {}", tmp_file_loc.display());
        } else {
            std::fs::remove_file(&tmp_file_loc)?;
        }
        eprintln!("finished post-cleanup");
        self.mark_installed()
    }

    fn sdk_dir(&self) -> std::path::PathBuf {
        let mut result = self.dest.clone();
        result.push(os_detect::vulkan_sdk_dir());
        result
    }

    pub fn cargo_env(&self) -> Vec<(&'static str, String)> {
        let sdk_path = self.sdk_dir();
        let sdk_path_str = sdk_path.display();

        let mut result = vec!(
            ("VULKAN_SDK", sdk_path_str.to_string()),
            ("PATH", join_env("PATH", format!("{}/bin", sdk_path_str)))
        );
        result.append(&mut os_detect::cargo_env(&sdk_path));

        result
    }

    pub fn print_cargo_env(&self) {
        for (k, v) in self.cargo_env().iter() {
            println!("cargo:rustc-env={}={}", k, v);
        }
    }

    pub fn write_cargo_env<W: std::io::Write>(&self, mut out: W) {
        for (k, v) in self.cargo_env().iter() {
            out.write_all(format!("export {}={}\n", k, v).as_bytes())
                .expect("failed to write env var");
        }
    }

    pub fn write_env_script(&self) {
        let mut script_path = self.sdk_dir();
        script_path.push("env.sh");

        eprintln!("env script at: {}", script_path.display());

        let file = std::fs::OpenOptions::new()
            .write(true)
            .truncate(true)
            .create(true)
            .open(script_path.display().to_string())
            .expect("failed to open/create vulkan env file");
        self.write_cargo_env(file);
    }

    fn make_fetch_url(ver: &String) -> String {
        format!(
            "https://sdk.lunarg.com/sdk/download/{ver}/{os_short}/{file}?Human=true",
            ver=ver,
            os_short=os_detect::os_name(),
            file=os_detect::vk_versioned_filename(ver),
        )
    }
}

pub fn ensure(ver: String) -> Result<SDK, Error> {
    eprintln!("handling Vulkan SDK version: {}", &ver);

    let sdk = SDK::with_version(&ver)?;
    sdk.install()?;

    Ok(sdk)
}
